# Ansible Role: isleward

* Installs prereqs needed to manage the server

## Requirements

  CentOS 7

## Role Variables

Yum packages installed as prereqs for isleward

```yml
yum_packages:
  - vim
  - htop
```

## Dependencies

  None

## Example Playbook

```yml
- hosts: localhost
  roles:
    - isleward.isleward
```

## Author Information

This role was created in 2018 by Vildravn.